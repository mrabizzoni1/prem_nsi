---
author: Maud RABIZZONI
title: Introduction
---

# Introduction

### Historique 
En 1989, Guido van Rossum, profitant d’une semaine de vacances durant les fêtes de Noël, utilise son ordinateur personnel pour écrire la première version du langage. Fan de la série télévisée Monty Python's Flying Circus, il décide de baptiser ce projet Python.
Python est un langage multiplateforme, c'est-à-dire disponible sur plusieurs architectures (compatible PC, tablettes, smartphones, Raspberry Pi...) et systèmes d'exploitation (Windows, Linux, Mac, Android...).
Le langage Python est gratuit, sous licence libre. 


### Open Source 
Python est libre et gratuit même pour les usages commerciaux. De nombreux outils standard sont disponibles.
 
### Travail interactif 
-	Nombreux interpréteurs interactifs disponibles (Pyzo, IPython, …) 
-	Importantes documentations en ligne 
-	Développement rapide et incrémentiel 
-	Tests et débogage outillés 
-	Analyse interactive de données 


### Simplicité du langage
-	Syntaxe claire et cohérente 
-	Indentation significative
-	Gestion automatique de la mémoire 
-	Typage dynamique fort : pas de déclaration 


### Disponibilité de bibliothèques 
Plusieurs milliers de packages sont disponibles dans tous les domaines 
