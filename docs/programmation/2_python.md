---
author: Maud RABIZZONI
title: Python
---



## 1. Fonctionnement du cours 

Trois grands types d'exercices sont proposés.

!!! exo "Papier/Crayon"

    Prenez une feuille et un crayon. 

!!! exo "Prédire/comprendre"

    Vous disposez d'un programme dans un éditeur. Vous devez comprendre le programme et prédire ce qui va se passer. Vous pouvez tester en appuyant sur la flèche pointant à droite.

    {{IDEv('exemples/exemple')}}

???+ question "Programmer"

    Vous devez compléter ou écrire un programme dans un éditeur. 

    Vous pouvez tester en appuyant sur la flèche pointant à droite ▶️. 


    {{IDEv('exemples/exemple2')}}

## 2. Interpréteur/Editeur

L’interpréteur (terminal ou shell), on le reconnait facilement car contient le triple chevron >>> qui est l’invite de Python et qui signifie que Python attend une commande. On peut taper des lignes de codes qui sont exécutées instantanément lorsqu’on valide par la touche Entrée.

!!! example "EXEMPLE"
    Taper 2+3 puis valider avec la touche ++enter++
    {{terminal()}}

    Dans l’éditeur, on peut y écrire des scripts, c’est-à-dire des programmes petits ou grands. Pour exécuter le script, il faut cliquer sur la flèche

    {{IDEv('exemples/exemple3')}}

## 3. Premiers pas

???+ question "EXERCICES"
    === "Exercice_1"
        Dans la console, taper `#!python print("Hello world !")` et appuyez sur ++enter++

    === "Exercice_2"
        Dans la console, taper `#!python "Hello world !"` et appuyez sur ++enter++
 
    === "Exercice_4"
        Dans la console, taper 32+5/2+1.5 et appuyez sur ++enter++

    {{terminal()}}

???+ question "EXERCICES"
    === "Exercice_5"
        Dans l'éditeur, taper `#!python print("Hello world !")` puis exécuter en cliquant sur ▶️.
    
    === "Exercice_6"
        Dans l'éditeur, taper `#!python 32+5/2+1.5` puis exécuter en cliquant sur ▶️.
    
    === "Exercice_7"
        Dans l'éditeur, taper `#!python print(32+5/2+1.5)` puis exécuter en cliquant sur ▶️.

    {{IDEv()}}


## 4. Les variables

### 4.1. Les noms


Une variable, dans le domaine de la programmation informatique, est un conteneur qui sert à stocker une valeur.

Les variables possèdent deux caractéristiques fondamentales :  
- Les variables ont une durée de vie limitée (une variable ne vit généralement que le temps de l’exécution d’un script ou de la fonction qui l’a définie);  
- La valeur d’une variable peut varier : les variables peuvent peuvent stocker différentes valeurs au cours de leur vie (la nouvelle valeur remplaçant l’ancienne).  

Le nom d'une variable s'écrit avec des lettres (non accentuées de préférence), des chiffres ou bien un underscore " _ ".   
Le nom d'une variable ne doit pas commencer par un chiffre.


!!! warning "Attention"
    Il existe des mots réservés. En voici quelques-uns :
    `#!python and, as, assert, break, class, continue, def, del, elif, else, except, False, finally, for, from, global, if, import, in, is, lambda, None, nonlocal, not, or, pass, raise, return, True, try, while, with, yield`

Notez que les noms de variables en Python sont sensibles à la casse, ce qui signifie qu’on va faire une différence entre l’emploi de majuscules et de minuscules : un même nom écrit en majuscules ou en minuscules créera deux variables totalement différentes.

???+ question "VRAI/FAUX"

    Cocher les identifiants valides.

    === "Question"
        - [ ] `toto`
        - [ ] `t0t0`
        - [ ] `3eme`
        - [ ] `Sa_3m7`
        - [ ] `else`
        - [ ] `Test()`
        - [ ] `Mon_Mot`
        - [ ] `Mon mot`
        - [ ] `_avant`
        - [ ] `login@mail`
        - [ ] `après`
        - [ ] `login.mail`
        - [ ] `login_mail`
        - [ ] `_`

    === "Solution"
        - [x] `toto`
        - [x] `t0t0`
        - [ ] `3eme` ; commence par un chiffre.
        - [x] `Sa_3m7`
        - [ ] `else` ; mot réservé.
        - [ ] `Test()` ; interdit d'utiliser les parenthèses.
        - [x] `Mon_Mot`
        - [ ] `Mon mot` ; pas d'espace.
        - [x] `_avant`
        - [ ] `login@mail` ; pas de @.
        - [ ] `après` ; non conforme mais valide
        - [ ] `login.mail` ; pas de point.
        - [x] `login_mail`
        - [x] `_`


### 4.2. L'affectation

Pour affecter ou assigner une valeur à une variable, nous allons utiliser un opérateur qu’on appelle opérateur d’affectation ou d’assignation et qui est représenté par le signe `=`. 

!!! warning "Attention"
    Lee signe `=` ne signifie pas en informatique l’égalité d’un point de vue mathématique : c’est un opérateur d’affectation.

!!! info "INFO"
    En python, une variable contient une référence (une sorte d'adresse) vers un objet contenant une valeur.
    La fonction `id(nom_variable)` renvoie la référence de la variable `nom_variable`.


On différenciera :  
- L'initialisation : c'est l'association initiale d'un contenu avec une valeur (1ère valeur) ;  
- L'affectation : c'est l'association d'un contenu avec une variable déjà initialisée ;  
- L'incrémentation : c'est l'augmentation régulière de la valeur associée à une variable .


!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire0')}}

!!! info "REMARQUE"
    En Python, de nombreux objets sont IMMUABLES (on ne peut pas les modifier). Cela se traduit par un changement de référence.


## 5. Les types

### 5.1. Le type int (integer : nbs entiers)

En Python, les variables ont toutes un type qui correspend à la nature de la valeur stockée dans la variable : nombre entier, nombre réel, chaîne de caractères, booléen (Vrai ou Faux), …

Contrairement à beaucoup d'autres langages, en Python, il n'est pas nécessaire de déclarer le type des variables lors de leur création : Python attribue automatiquement un type à une variable lors de sa création, on parle de typage dynamique.



???+ question "EXERCICE"
    Dans l'éditeur, créer une variable age en lui donnant la valeur 15.  
    Afficher dans la console la valeur de la variable en utilisant la fonction `print(nom_de_la_variable)`.  
    Taper  `print(type(age))`  puis executer.

    {{IDEv()}}



!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire1')}}
	

Sur ce type de variables, on peut faire différentes opérations :  
- L’addition avec le symbole « + ».  
- La soustraction avec le symbole « - ».  
- La multiplication avec le symbole « * ».  
- La division avec le symbole « / ».  
- La division entière avec le symbole « // » (le résultat est la partie entière de la division).  
- Le modulo avec le symbole « % » (le résultat est le reste de la division).  
- La puissance avec le symbole « ** ».  

???+ question "EXERCICE"
    Ecrire les lignes de codes pour répondre au cahier des charges suivant :  
    Créer une variable `a = 6*3+2`.  
    Créer une variable `b = 20`.  
    Créer une variable `c = a+2*b`.  
    Afficher dans la même ligne `a`, `b` et `c`.  
    Créer une variable `angle = 450`.  
    Créer une variable `tour` et calculer le nombre de tour entier qu’il y a dans angle.  
    Créer une variable `reste` et calculer l’angle restant (on utilisera `%`).  
    Afficher sur la même ligne `angle`, `tour` et `reste`.  
    Créer une variable `puissance` qui vaut 2 exposant 20 et afficher le résultat et son type.  
    Créer une variable `racine2` qui sera la racine carrée de 2.

    {{IDEv()}}

    ??? success "Solution"
        ```python
        a=6*3+2
        b=20
        c=a+2*b
        print(a,b,c)

        angle=450
        tour=angle//180
        reste=angle%180
        print(angle,tour,reste)
        puissance=2**20
        print(puissance,type(puissance))
        racine2=2**0.5 # ou racine2=sqrt(2) 
        ```



### 5.2. Le type float (floating : nombres en virgule flottante)

Pour déclarer un float, il suffit de mettre le séparateur décimal qui est un point sous python.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire2')}}


On peut aussi écrire des réels au format scientifique avec la lettre e.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire3')}}


On peut aussi utiliser des fonctions mathématiques existantes. Pour cela, il faut importer le module math.  
Pour connaitre toutes les fonctions d’un module, il suffit d’utiliser la fonction `dir(nom du module)`.  
Pour utiliser une fonction d’un module, il suffit d'importer le module (ou librairie ou bibliothèque).

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire4')}}


???+ question "EXERCICE"
    Créer une variable Pi et lui affecter la valeur $\Pi$.  
    Créer la variable cosPi est lui affecter la valeut de cosinus $\Pi$ .

    {{IDEv('scripts/exo1')}}



### 5.3. Le type str (string : chaîne de caractères)

Pour créer une variable str, il faut l’écrire entre qotes « `'` » ou double quotes « `"` » .

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire5')}}  

On peut concaténer deux chaînes à l’aide de l’opérateur « `+` ».  
On peut connaitre la taille d’une chaîne à l’aide de la fonction `len( nom_de_la_chaîne )`.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire6')}}

Que faire lorsqu’on a une apostrophe dans une variable str ?

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire7')}}


!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire8')}}

 
La séquence d'échappement `\n` représente un saut ligne. 


### 5.4. Le type bool (boolean : vrai ou faux)

Cette variable peut avoir deux valeurs :  
 - True (vrai);  
 - False (faux).  

Avec ces variables, on peut utiliser les opérateurs de comparaisons :  

| Opérateur | Signification         |
|-----------|-----------------------|
|     <     | strictement inférieur |
|     <=    | inférieur ou égal     |
|     >     | strictement supérieur |
|     >=    | supérieur ou égal     |
|     ==    | égal                  |
|     !=    | différent             |

On peut aussi utiliser les opérateurs logiques `and`, `or` et `not`.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDE('predires/predire11')}}

  

On peut aussi utiliser l’opérateur `in` avec les chaînes de caractères.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDEv('predires/predire12')}}


???+ question "QCM"
    Quel est le type des données et des expressions suivantes :

    Question 1 : 3.14
    {{ qcm(["int", "float", "str","bool","Aucun"], [2], shuffle = False) }}


    Question 2 : 2e10
    {{ qcm(["int", "float", "str","bool","Aucun"], [1], shuffle = False) }}

    Question 3 : "200"
    {{ qcm(["int", "float", "str","bool","Aucun"], [3], shuffle = False) }}

    Question 4 : 2,10
    {{ qcm(["int", "float", "str","bool","Aucun"], [5], shuffle = False) }}

    Question 5 : 204/5
    {{ qcm(["int", "float", "str","bool","Aucun"], [2], shuffle = False) }}

    Question 6 : 204%5
    {{ qcm(["int", "float", "str","bool","Aucun"], [1], shuffle = False) }}

    Question 7 : 20//3
    {{ qcm(["int", "float", "str","bool","Aucun"], [1], shuffle = False) }}

    Question 8 : 1.0==1
    {{ qcm(["int", "float", "str","bool","Aucun"], [4], shuffle = False) }}

    Question 9 : a=2 est une :
    {{ qcm(["affectation", "comparaison","Aucun"], [1], shuffle = False) }}

    Question 10 : a==2 est une :
    {{ qcm(["affectation", "comparaison","Aucun"], [2], shuffle = False) }}




    
### 5.5. Le transtypage

Le transtypage désigne la conversion d’une variable, plus globalement d’une expression, d’un type en une expression d’un autre type. 

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDEv('predires/predire9')}}

???+ question "EXERCICE"
    Créer une variable b contenant l'entier 22, en utilisant uniquement la variable `a` initialisée à 2, les fonctions de transtypage et une seule fois l'opérateur `+`.

    {{IDEv('scripts/exo10')}}


???+ question "QCM"
    Quel est le type des données ou des expressions suivantes :

    Question 1 : str(2)
    {{ qcm(["int", "float", "str","bool","Erreur"], [3], shuffle = False) }}

    Question 2 : int(2.5)
    {{ qcm(["int", "float", "2","2.5","Erreur"], [1,3], shuffle = False) }}

    Question 3 : int('2.5')
    {{ qcm(["int", "float", "str","bool","Erreur"], [5], shuffle = False) }}

    Question 4 : float('7.4')
    {{ qcm(["int", "float", "str","bool","Erreur"], [2], shuffle = False) }}

    Question 5 : "3.14"+str(2)
    {{ qcm(["5.14", "3.142","Erreur"], [2], shuffle = False) }}

    Question 6 : float("2.1")+2
    {{ qcm(["2.12", "4.1","Erreur"], [2], shuffle = False) }}

## 6. La fonction input()

La fonction `input()` lance une invite de commande (en anglais : prompt) pour saisir une chaîne de caractères.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDEv('predires/predire10')}}


???+ question "EXERCICES"
    === "Exercice 1"
        Demander son nom à l'utilisateur, le résultat sera stocké dans une variable `nom`;  
        Demander son prénom à l'utilisateur, le résultat sera stocké dans une variable `prenom`;  
        Afficher : Bonjour `prenom nom`

        {{IDEv()}}

        ??? success "Solution"
            ```python
            prenom=input("Quel est ton prénom ? ")
            nom=input("Quel est ton nom ? ")
            print("Bonjour",prenom,nom)
            ```

    === "Exercice 2"
        Demander son âge à l'utilisateur, le résultat sera stocké dans une variable `age`;  
        Afficher : Tu as `age` ans  
        Incrémenter `age` de 1 puis afficher : l'année prochaine tu auras ... ans.

        {{IDEv()}}

        ??? success "Solution"
            ```python
            age=int(input("Quel est ton âge ? "))
            age=age+1
            print("l'année prochaine tu auras",age,"ans")
            ```
    === "Exercice 3"
        Écrire un script qui demande la saisie d'un prix hors taxe, puis calcule le prix TTC (TVA de 5.5%).

        {{IDEv('scripts/exo3')}}

    === "Exercice 4"
        Écrire un script qui demande la saisie du nombre d'élève, du nombre d'absents et affiche le pourcentage d'absentéisme.

        {{IDEv('scripts/exo4')}}
    
    === "Exercice 5"
        Écrire un script qui demande la saisie d'un nombre de seconde et affiche cette durée au format heures/minutes/secondes.

        {{IDEv('scripts/exo5')}}   



## 7. Les séquences

!!! abstract "DEFINITION"
    Une séquence est un conteneur ordonné d’éléments indexés par des entiers indiquant leur position dans le conteneur.  
    Python dispose de trois types prédéfinis de séquences :  
	    — les chaînes (vues précédemment) ;   
	    — les listes ;   
	    — les tuples  

!!! danger "SYNTAXE"
    Si on veut supprimer, remplacer ou insérer plusieurs éléments d’une séquence, on utilisera l’opérateur d’indexage :  
    <div style="text-align:center;">`Nom[début : fin : pas]`</DIV>

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :
    {{IDEv('predires/predire16')}}

REMARQUE :  
<div style="text-align:center;">
<TABLE>
<table>
    <tr>
        <td>B</td>
        <td>o</td>
        <td>n</td>
        <td>j</td>
        <td>o</td>
        <td>u</td>
        <td>r</td>
        <td></td>
        <td>l</td>
        <td>e</td>
        <td></td>
        <td>m</td>
        <td>o</td>
        <td>n</td>
        <td>d</td>
        <td>e</td>
        <td></td>
        <td>l</td>
    </tr>
    <tr>
        <td>0</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
        <td>10</td>
        <td>11</td>
        <td>12</td>
        <td>13</td>
        <td>14</td>
        <td>15</td>
        <td>16</td>
        <td>17</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td>...</td>
        <td></td>
        <td>-4</td>
        <td>-3</td>
        <td>-2</td>
        <td>-1</td>
    </tr>
</table>
</div>



### 7.1. Le type tuple 

!!! abstract "DEFINITION"
    Un tuple est une collection ordonnée et non modifiable d’éléments éventuellement hétérogènes. 

!!! danger "SYNTAXE" 
    Éléments séparés par des virgules, et entourés de parenthèses.  
    exemple : `mon_tuple=(1,2,3)`


???+ question "EXERCICE"
    Coder les lignes suivantes :  
    mon_tuple = (5, 8, 6, 9).  
    Afficher le type.    

    {{IDEv()}}

    ??? success "remarques"
        La variable mon_tuple référence un tuple, ce tuple est constitué des entiers 5, 8, 6 et 9.  
        Chaque élément du tuple est indicé (ou indexé) :  
        - le premier élément du tuple (l'entier 5) possède l'indice 0  
        - le deuxième élément du tuple (l'entier 8) possède l'indice 1  
        ... 




### 7.2. Le type list 

Il n'est pas possible de modifier un tuple après sa création (on parle d'objet "immuable"), si vous essayez de modifier un tuple existant, l'interpréteur Python vous renverra une erreur. 

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire14')}}

!!! abstract "DEFINITION"
    Une liste est une collection ordonnée et modifiable d’éléments éventuellement hétérogènes. 

!!! danger "SYNTAXE"  
    Éléments séparés par des virgules, et entourés de crochets.
    exemple : `L=[1,2,"trois"]`

Les listes sont, comme les tuples, des séquences, mais à la différence des tuples, elles sont modifiables (on parle d'objets "mutables").

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire15')}}



On peut faire des listes de listes.  
Nous obtenons ainsi quelque chose qui ressemble beaucoup à un "objet mathématique" très utilisé : une matrice.  
Il est évidemment possible d'utiliser les indices de position avec ces "listes de listes".  
Pour cela nous allons considérer notre liste de listes comme une matrice, c'est à dire un tableau.

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire17')}}

Une représentation matricielle de m :  
<div style="text-align:center;">
<TABLE>
<TR><TD>1</TD><TD>3</TD><TD>4</TD></TR>
<TR><TD>5</TD><TD>6</TD><TD>8</TD></TR>
<TR><TD>2</TD><TD>1</TD><TD>3</TD></TR>
<TR><TD>8</TD><TD>8</TD><TD>15</TD></TR>
</TABLE>
</div>


Pour cibler un élément particulier de la matrice, on utilise la notation avec "doubles crochets" : `m[ligne][colonne]` (la première ligne et la première colonne ont pour indice 0).

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire18')}}

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDEv('predires/predire13')}}


???+ question "QCM"
    Déterminer, à l'aide de l'opérateur d'indiçage [] les expressions qui correspondent aux ensembles d'indices suivants :

    Question 1 : L'indices du 3ème élément de la séquence
    {{ qcm(["[2]", "[3]", "[4]","Aucun"], [1], shuffle = False) }}

    Question 2 : Tous les indices à partir du début, jusqu’à la fin de la séquence, avec un pas de 3
    {{ qcm(["[0::3]", "[::3]", "[0,3]","Aucun"], [1,2], shuffle = False) }}

    Question 3 : Tous les indices à partir de 3, jusqu’à la fin de la séquence.
    {{ qcm(["[3]", "[3:]", "[3::]","[3::1]"], [2,3,4], shuffle = False) }}
    
    Question 4 : L’indice 2 dans la séquence d’indice 3 de la séquence.
    {{ qcm(["[2][3]", "[2,3]", "[3][2]","Aucun"], [3], shuffle = False) }}
    
    Question 5 : L'indices du dernier élément de la séquence
    {{ qcm(["[0]", "[len]", "[-1]","Aucun"], [3], shuffle = False) }}


???+ question "EXERCICE"
    Compléter le script si cela est possible, sinon mettre la variable à False.    

    {{IDE('scripts/exo7')}}


On pourrait s'interroger sur l'intérêt d'utiliser un tuple puisque la liste permet plus de choses ! La réponse est simple : les opérations sur les tuples sont plus "rapides". Quand vous savez que votre liste ne sera pas modifiée, il est préférable d'utiliser un tuple à la place d'une liste.


## 8. Les fonctions

Les fonctions sont les éléments structurants de base de tout langage procédural.  
Elles offrent différents avantages :  
-	Évite la répétition : on peut « factoriser » une portion de code qui se répète lors de l’exécution en séquence d’un script ;  
-	Met en relief les données et les résultats : entrées et sorties de la fonction ;  
-	Permet la réutilisation : mécanisme de l’import ;  
-	Décompose une tâche complexe en tâches plus simples : conception de l’application.  

### 8.1. Définition et syntaxe

!!! abstract "DEFINITION"
    Une fonction est un ensemble d’instructions regroupées sous un nom et s’exécutant à la demande.  
    On doit définir une fonction à chaque fois qu’un bloc d’instructions se trouve à plusieurs reprises dans le code ; il s’agit d’une « mise en facteur commun ». 

!!! danger "SYNTAXE" 
    La définition d’une fonction est composée :  
	- du mot clé `def` suivi de l’identificateur de la fonction, de parenthèses entourant les paramètres de la fonction séparés par des virgules, et du caractère « deux points » qui termine toujours une instruction composée ;  
    - d’une chaîne de documentation indentée comme le corps de la fonction (appelé docstring) ;   
    - du bloc d’instructions indenté par rapport à la ligne de définition, et qui constitue le corps de la fonction.

Le bloc d’instructions est obligatoire. S’il est vide, on emploie l’instruction pass. La documentation (facultative) est fortement conseillée. 

!!! example "EXEMPLE"
    ```python
        def DivEucl(a, b) :
            """Renvoie le quotient et le reste de a par b""" 
	        q = a // b 
	        r = a % b 
	        return q,r
    ```



### Passage des arguments 

Chaque argument de la définition de la fonction correspond, dans l’ordre, à un paramètre de l’appel. La correspondance se fait par affectation des paramètres aux arguments. 


### Un ou plusieurs paramètres, pas de retour.

Sans l’instruction `return`, ce qu’on appelle souvent une procédure. Dans ce cas la fonction renvoie implicitement la valeur None 

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire19')}}
    ???+ success "REMARQUES"
        `retour = table(5,1,9)`  
        La procédure `table` ne renvoie rien donc la variable `retour` est initialisée à `None`.  
        Par contre, cette procédure contient un `print` dans son bloc instructions d'où l'affichage.


### Un ou plusieurs paramètres, un ou plusieurs retours 

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire20')}}


## 9. Les structures de contrôle

Pour identifier les instructions composées, Python utilise la notion d’indentation significative. Cette syntaxe, légère et visuelle, met en lumière un bloc d’instructions et permet d’améliorer grandement la présentation et donc la lisibilité des programmes sources. 

!!! danger "SYNTAXE"  
    Une instruction composée se compose :  
    — d’une ligne d’en-tête terminée par deux-points ;  
    — d’un bloc d’instructions indenté par rapport à la ligne d’en-tête. 

!!! warning "ATTENTION" 
    Toutes les instructions au même niveau d’indentation appartiennent au même bloc. 



## 9.1. La structure conditionnelle IF

!!! danger "STRUCTURE" 
    ```python
        if  test_conditionnel_1 :
            bloc_instructions_1
        [elif test_conditionnel_2 :
	        bloc_instructions_2]
        [else : 
            bloc_instructions_3]
    ```
    Lorsque que le bloc est entouré de crochets, cela signifie que le bloc est facultatif.



Comment cela fonctionne-t-il ?  
Si `test_conditionnel_1` est `True` alors  
        `bloc_instructions_1` est exécuté,  
Sinon si `test_conditionnel_2` est `True` alors  
        `bloc_instructions_1` est ignoré et `bloc_instructions_2` est exécuté,  
Sinon (sous-entendu que `bloc_instructions_1` et `bloc_instructions_2` sont `False`)  
        `bloc_instruction_3` est exécuté et, `bloc_instructions_1` ainsi `bloc_instructions_2` sont ignorés

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant avec les valeurs 33, 18 et 8 pour note :

    {{IDE('predires/predire21')}}



Pour traiter le cas des notes invalides (<0 ou >20), on peut imbriquer des instructions conditionnelles

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant avec les valeurs 33, 20, 0 et 8 pour note :

    {{IDE('predires/predire22')}}

!!! exo "Prédire/comprendre"

    Interpréter les résultats du programme suivant avec les valeurs 33, 20, 0, 18 et 8 pour note :

    {{IDE('predires/predire23')}}

???+ question "EXERCICE"
    Ecrire le script qui calcule de l’IMC d’un adulte = Masse/taille² en kg/m² et qui l’interprète suivant le tableau ci-dessous :
    IMC (kg·m²)	Interprétation
    moins de 16,5	dénutrition ou famine
    16,5 à 18,5	maigreur
    18,5 à 25	corpulence normale
    25 à 30	surpoids
    30 à 35	obésité modérée
    35 à 40	obésité sévère
    plus de 40	obésité morbide ou massive   

    {{IDE()}}

    ??? success "Correction"
        A FAIRE




## 9.2. La boucle while 

Une boucle permet d'exécuter une portion de code plusieurs fois de suite.

!!! danger "STRUCTURE" 
    ```python
        while  condition(s) :
            bloc_instructions
    ```
    

Tant que la condition reste vraie, les instructions à l'intérieur du bloc (indentation) seront exécutées.



!!! exo "Prédire/comprendre"
    === "ex1"

        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire24')}}
    
    === "ex2"
        
        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire25')}}

    === "ex3"
     
        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire26')}}

    === "ex4"
     
        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire27')}}




## 9.3. La boucle for 

De façon générale, nous parlerons de conteneur pour désigner un type de données permettant de stocker un ensemble d’autres données, en ayant ou non, suivant les types, une notion d’ordre entre ces données.   
Nous parlerons aussi d’itérable pour désigner un conteneur que l’on peut parcourir élément par élément. Pour parcourir ces conteneurs, nous nous servirons parfois de l’instruction range() qui fournit un moyen commode pour générer une liste de valeurs.  

EXEMPLE
```python
>>> uneListe = list(range(6)) 
>>> uneListe 
[0, 1, 2, 3, 4, 5] 
```


!!! danger "STRUCTURE" 
    ```python
        for i in J :		   
        # i est une identificateur 
        # et J une séquence (liste, chaîne de caractères,…)
            bloc_instructions
    ```  


!!! exo "Prédire/comprendre"
    === "ex1"
        Exemple avec une séquence de caractères

        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire29')}}
    
    === "ex2"
        Exemple avec les éléments d'une liste

        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire30')}}

    === "ex3"
        Fonction range()

        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire31')}}

    === "ex4"
        Table de multiplication

        Interpréter les résultats du programme suivant :

        {{IDE('predires/predire32')}}

La variable lettre est initialisée avec le premier élément de la séquence : `B`.
Le bloc d'instructions est alors exécuté.
Puis la variable lettre est mise à jour avec le second élément de la séquence `o` et le bloc d'instructions à nouveau exécuté...
Le bloc d'instructions est exécuté une dernière fois lorsqu'on arrive au dernier élément de la séquence.  


## 9.4. L'instruction break

L'instruction break provoque une sortie immédiate d'une boucle while ou d'une boucle for.
Dans l'exemple suivant, l'expression True est toujours ... vraie : on a une boucle sans fin.
L'instruction break est donc le seul moyen de sortir de la boucle.

!!! exo "Prédire/comprendre"
    Interpréter les résultats du programme suivant :

    {{IDE('predires/predire33')}}


## 10. EXERCICES

???+ question "EXERCICES"
    === "Exercice 1"
        Écrivez un programme qui affiche une suite de 12 nombres dont chaque terme est égal au triple du terme précédent.

        {{IDEv()}}

    === "Exercice 2"
        Un résultat surprenant !  
        On dispose d'une feuille de papier d'épaisseur 0,1 mm. 
        Combien de fois doit-on la plier au minimum pour que l'épaisseur dépasse la hauteur de la tour Eiffel 324 m. 
        Écrire un programme en Python pour résoudre ce problème.

        {{IDEv()}}
    
    === "Exercice 3" 
        Comprendre la différence entre for et while!
        Aujourd'hui un appartement vaut 100 000€. Sa valeur augmente de 1% chaque année.
        1. Écrire un programme en Python pour connaître sa valeur au bout de 10 ans.
        2. Écrire un programme en Python pour savoir au bout de combien d'années sa valeur aura doublé.

        {{IDEv()}}

    === "Exercice 4"
        Inès veut construire une pyramide à base carrée.
        Inès a 1000 billes. Combien d'étages au maximum aura sa pyramide ? 
        Écrire un programme en Python pour répondre au problème.

        {{IDEv()}}	

    === "Exercice 5" 
        Jeu du plus ou moins
        L'ordinateur tire un nombre entier au hasard entre 0 et 100.
        L'utilisateur doit le trouver et pour cela propose des valeurs.
        L'ordinateur indique pour chaque valeur proposée si la valeur est trop petite, trop grande ou s'il a trouvé.
        1. Écrire un programme en Python pour jouer à ce jeu. En combien de coups est-on sûr de trouver ?
        2. Modifier le programme pour qu'il s'arrête si l'utilisateur n'a pas trouvé au bout du nombre de coups maximum.

        {{IDEv()}}











