variable = True  
print(variable)   
print(type(variable))   

b = 10   
print(b > 8)   
print(b == 5)   
print(b != 10)   
print(0 <= b <= 20)   

note = 13.0   
mention_ab = note >= 12.0 and note < 14.0 
# ou bien : mention_ab = 12.0  <= note < 14.0   
print(mention_ab)   
print(not mention_ab)   
print(note == 20.0 or note == 0.0)   