ma_liste = [5, 8, 6, 9] 
print(type(ma_liste))  

print(ma_liste[2]) 

ma_liste[2] = 15 #on remplace l’élément d’indice 2 par 15 
print(ma_liste)

ma_liste = ma_liste + [15] 
# le « + » est le symbole de concaténation, ce qui a pour effet d’ajouter l’élément 
print(ma_liste)

ma_liste.append(33) #on ajoute un élément 33 car la liste est mutable 
print(ma_liste) 

del ma_liste[1] #on supprime l’élément d’indice 1 
print(ma_liste) 

print(len(ma_liste)) #on affiche la taille de la liste 
print(ma_liste) 