def table(base, debut, fin) :
	"""Affiche la table de multiplication de base de debut à fin.""" 
	n = debut
	while n <= fin :
		print(n, 'x', base, '=', n * base)
		n += 1 

retour = table(5,1,9)
print(retour)