from math import pi 

def cube(x) :
    """Retourne le cube de l’argument x""" 
    return x**3 

def volumeSphere(r) :
    """Retourne le volume d'une sphère de rayon r""" 
    return 4.0 * pi * cube(r) / 3.0 	

# Saisie du rayon et affichage du volume 
rayon = float(input('Rayon : ')) 
print("Volume de la sphère =", volumeSphere(rayon)) 