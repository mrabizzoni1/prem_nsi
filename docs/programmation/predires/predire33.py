import time     # importation du module time
while True:
    # strftime() est une fonction du module time
    print ('Heure courante ',time.strftime('%H:%M:%S'))
    quitter = input('Voulez-vous quitter le programme (o/n) ? ')
    if quitter == 'o':
        break
print ('A bientôt')
