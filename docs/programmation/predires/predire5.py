nom = 'Dupont'  # entre apostrophes  
print(nom)   
print(type(nom))   

prenom = "Pierre" 	# on peut aussi utiliser les guillemets  
print(prenom)

print(nom, prenom) # ne pas oublier la virgule