prenom='Timothée'
nom='Rabibi'

chaine = nom + prenom	# concaténation 2 chaînes  
print(chaine)

chaine = prenom + nom	# concaténation 2 chaînes  
print(chaine)

chaine = prenom + ' ' + nom 
print(chaine)

chaine = chaine + ' à 3 ans'	# en plus court : chaine += ' 18 ans' 
print(chaine)

print(len(chaine)) 

print('Bien'*3)