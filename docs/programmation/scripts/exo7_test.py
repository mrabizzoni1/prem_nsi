

assert T1 == T[1::2]
assert T2 == T[3::]
assert T3 == T[5][2]
assert T4 == False

assert L1 == L[::2]
assert L2 == L[-3::]
assert L3 == L[3][2]
assert L4 == L[::-1]